package com.grup2.urlshortener.urlshortener.models;

import com.grup2.urlshortener.urlshortener.contracts.request.AddLinkRequest;
import com.grup2.urlshortener.urlshortener.contracts.request.UpdateLinkRequest;
import com.grup2.urlshortener.urlshortener.contracts.request.UpdateUserRequest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class UserTests {

    User sut;
    List<Link> links;
    Link link1;
    Link link2;
    Link link3;
    List<String> tags1;
    List<String> tags2;
    List<String> tags3;
    String tag1;
    String tag2;
    String tag3;
    String tag4;
    String tag5;

    @BeforeEach
    public void setup() {
        links = new ArrayList<>();

        tag1 = "education";
        tag2 = "school";
        tag3 = "job";
        tag4 = "old";
        tag5 = "new";

        tags1 = new ArrayList<>();
        tags1.add(tag1);
        tags1.add(tag5);

        tags2 = new ArrayList<>();
        tags2.add(tag2);
        tags2.add(tag4);

        tags3 = new ArrayList<>();
        tags3.add(tag1);
        tags3.add(tag2);
        tags3.add(tag5);

        link1 = new Link("1a45cca","www.google.com", "123456",null, "first link",tags1,0);
        link2 = new Link("14dcs44","www.tredyol.com", "",null, "second link",tags2,5);
        link3 = new Link("csac45a","www.gtu.edu.tr", "111111",null, "third link",tags3,10);
    }

    @AfterEach
    public void cleanup() {
        sut = null;
        links = new ArrayList<>();
    }

    @Test
    public void updateByRequest_WhenUpdateUserRequestNull_ShouldNotChangeAnything() {
        //Arrange
        links.add(link1);
        links.add(link2);
        links.add(link3);

        sut = new User("1a45ccas4c5aadc3","mustafa","mustafaakilli@outlook.com","1a45ccas4c5aadc3",links,links.size());

        //Act
        sut.updateByRequest(null);

        //Assert
        assertThat(sut.getName()).isEqualTo("mustafa");
        assertThat(sut.getEmail()).isEqualTo("mustafaakilli@outlook.com");
    }

    @Test
    public void updateByRequest_WhenUpdateUserRequestHaveNullElements_ShouldNotChangeAnything() {
        //Arrange
        links.add(link1);
        links.add(link2);
        links.add(link3);

        sut = new User("1a45ccas4c5aadc3","mustafa","mustafaakilli@outlook.com","1a45ccas4c5aadc3",links,links.size());
        UpdateUserRequest updateUserRequest = new UpdateUserRequest(null,null);

        //Act
        sut.updateByRequest(updateUserRequest);

        //Assert
        assertThat(sut.getName()).isEqualTo("mustafa");
        assertThat(sut.getEmail()).isEqualTo("mustafaakilli@outlook.com");
    }

    @Test
    public void updateByRequest_WhenUpdateUserRequestHaveNewName_ShouldUpdateName() {
        //Arrange
        links.add(link1);
        links.add(link2);
        links.add(link3);

        sut = new User("1a45ccas4c5aadc3","mustafa","mustafaakilli@outlook.com","1a45ccas4c5aadc3",links,links.size());
        UpdateUserRequest updateUserRequest = new UpdateUserRequest("ahmet",null);

        //Act
        sut.updateByRequest(updateUserRequest);

        //Assert
        assertThat(sut.getName()).isEqualTo(updateUserRequest.getName());
    }

    @Test
    public void updateByRequest_WhenUpdateUserRequestHaveNotNewName_ShouldNotUpdateName() {
        //Arrange
        links.add(link1);
        links.add(link2);
        links.add(link3);

        sut = new User("1a45ccas4c5aadc3","mustafa","mustafaakilli@outlook.com","1a45ccas4c5aadc3",links,links.size());
        UpdateUserRequest updateUserRequest = new UpdateUserRequest(null,"ahmet@gmail.com");

        //Act
        sut.updateByRequest(updateUserRequest);

        //Assert
        assertThat(sut.getName()).isEqualTo("mustafa");
    }

    @Test
    public void updateByRequest_WhenUpdateUserRequestHaveNewEmail_ShouldUpdateEmail() {
        //Arrange
        links.add(link1);
        links.add(link2);
        links.add(link3);

        sut = new User("1a45ccas4c5aadc3","mustafa","mustafaakilli@outlook.com","1a45ccas4c5aadc3",links,links.size());
        UpdateUserRequest updateUserRequest = new UpdateUserRequest(null,"ahmet@gmail.com");

        //Act
        sut.updateByRequest(updateUserRequest);

        //Assert
        assertThat(sut.getEmail()).isEqualTo(updateUserRequest.getEmail());
    }

    @Test
    public void updateByRequest_WhenUpdateUserRequestHaveNotNewEmail_ShouldNotUpdateEmail() {
        //Arrange
        links.add(link1);
        links.add(link2);
        links.add(link3);

        sut = new User("1a45ccas4c5aadc3","mustafa","mustafaakilli@outlook.com","1a45ccas4c5aadc3",links,links.size());
        UpdateUserRequest updateUserRequest = new UpdateUserRequest("ahmet",null);

        //Act
        sut.updateByRequest(updateUserRequest);

        //Assert
        assertThat(sut.getEmail()).isEqualTo("mustafaakilli@outlook.com");
    }

    @Test
    public void addLink_WhenAddLinkRequestAdded_ShouldIncrease1LinkCount() {
        //Arrange
        links.add(link1);
        links.add(link2);
        links.add(link3);

        sut = new User("1a45ccas4c5aadc3","mustafa","mustafaakilli@outlook.com","1a45ccas4c5aadc3",links,links.size());
        AddLinkRequest addLinkRequest = new AddLinkRequest("www.tredyol.com",null,"123456", "second link",tags2);

        int beforeActLinkCount = sut.getLinkCount();

        //Act
        sut.addLink(addLinkRequest);

        //Assert
        assertThat(sut.getLinkCount()).isEqualTo(beforeActLinkCount + 1);
    }

    @Test
    public void addLink_WhenAddLinkRequestAdded_ShouldBeAddedToTheEndOfTheList() {
        //Arrange
        links.add(link1);
        links.add(link2);
        links.add(link3);

        sut = new User("1a45ccas4c5aadc3","mustafa","mustafaakilli@outlook.com","1a45ccas4c5aadc3",links,links.size());
        AddLinkRequest addLinkRequest = new AddLinkRequest("www.tredyol.com",null,"123456", "second link",tags2);

        //Act
        sut.addLink(addLinkRequest);

        //Assert
        assertThat(sut.getLinks().get(sut.getLinks().size() - 1).getTargetLink()).isEqualTo(addLinkRequest.getTargetLink());
        assertThat(sut.getLinks().get(sut.getLinks().size() - 1).getPassword()).isEqualTo(addLinkRequest.getPassword());
        assertThat(sut.getLinks().get(sut.getLinks().size() - 1).getDescription()).isEqualTo(addLinkRequest.getDescription());
    }

    @Test
    public void deleteLink_WhenLinkIDIsExist_ShouldDeleteLinkAndDecrease1LinkCount() {
        //Arrange
        links.add(link1);
        links.add(link2);
        links.add(link3);

        sut = new User("1a45ccas4c5aadc3","mustafa","mustafaakilli@outlook.com","1a45ccas4c5aadc3",links,links.size());

        int beforeActLinksSize = sut.getLinks().size();
        int beforeActLinkCount = sut.getLinkCount();

        //Act
        sut.deleteLink(link2.getId());

        //Assert
        assertThat(sut.getLinks().size()).isEqualTo(beforeActLinksSize - 1);
        assertThat(sut.getLinkCount()).isEqualTo(beforeActLinkCount - 1);
    }

    @Test
    public void deleteLink_WhenLinkIDIsNotExist_ShouldNotDeleteLinkAndDoNotChangeLinkCount() {
        //Arrange
        links.add(link1);
        links.add(link2);
        links.add(link3);

        sut = new User("1a45ccas4c5aadc3","mustafa","mustafaakilli@outlook.com","1a45ccas4c5aadc3",links,links.size());

        int beforeActLinksSize = sut.getLinks().size();
        int beforeActLinkCount = sut.getLinkCount();

        //Act
        sut.deleteLink("ignoreId");

        //Assert
        assertThat(sut.getLinks().size()).isEqualTo(beforeActLinksSize);
        assertThat(sut.getLinkCount()).isEqualTo(beforeActLinkCount);
    }

    @Test
    public void updateLink_WhenLinkIDIsExist_ShouldUpdateLink() {
        //Arrange
        links.add(link1);
        links.add(link2);
        links.add(link3);

        sut = new User("1a45ccas4c5aadc3","mustafa","mustafaakilli@outlook.com","1a45ccas4c5aadc3",links,links.size());
        UpdateLinkRequest updateLinkRequest = new UpdateLinkRequest(null,"123456", "updated link",tags2);

        //Act
        sut.updateLink(link1.getId(),updateLinkRequest);

        //Assert
        assertThat(sut.getLinks().get(0).getDescription()).isEqualTo(updateLinkRequest.getDescription());
    }

    @Test
    public void updateLink_WhenLinkIDIsNotExist_ShouldNotUpdateAnyLink() {
        //Arrange
        links.add(link1);
        links.add(link2);
        links.add(link3);

        sut = new User("1a45ccas4c5aadc3","mustafa","mustafaakilli@outlook.com","1a45ccas4c5aadc3",links,links.size());
        UpdateLinkRequest updateLinkRequest = new UpdateLinkRequest(null,"123456", "updated link",tags2);

        //Act
        sut.updateLink("ignoreId",updateLinkRequest);

        //Assert
        assertThat(sut.getLinks().get(0).getDescription()).isNotEqualTo(updateLinkRequest.getDescription());
        assertThat(sut.getLinks().get(1).getDescription()).isNotEqualTo(updateLinkRequest.getDescription());
        assertThat(sut.getLinks().get(2).getDescription()).isNotEqualTo(updateLinkRequest.getDescription());
    }

    @Test
    public void getLinkById_WhenLinkIDIsExist_ShouldGetLink() {
        //Arrange
        links.add(link1);
        links.add(link2);
        links.add(link3);

        sut = new User("1a45ccas4c5aadc3","mustafa","mustafaakilli@outlook.com","1a45ccas4c5aadc3",links,links.size());

        //Act
        Link link = sut.getLinkById(link2.getId());

        link2 = new Link("14dcs44","www.tredyol.com", "",null, "second link",tags2,5);

        //Assert
        assertThat(link.getId()).isEqualTo(link2.getId());
        assertThat(link.getTargetLink()).isEqualTo(link2.getTargetLink());
        assertThat(link.getDescription()).isEqualTo(link2.getDescription());
        assertThat(link.getUsageCounter()).isEqualTo(link2.getUsageCounter());
    }

    @Test
    public void getLinkById_WhenLinkIDIsNotExist_ShouldNotGetNull() {
        //Arrange
        links.add(link1);
        links.add(link2);
        links.add(link3);

        sut = new User("1a45ccas4c5aadc3","mustafa","mustafaakilli@outlook.com","1a45ccas4c5aadc3",links,links.size());

        //Act
        Link link = sut.getLinkById("ignoreId");

        //Assert
        assertThat(link).isEqualTo(null);
    }

}
