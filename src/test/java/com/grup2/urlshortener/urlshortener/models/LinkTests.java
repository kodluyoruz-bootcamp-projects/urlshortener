package com.grup2.urlshortener.urlshortener.models;

import com.grup2.urlshortener.urlshortener.contracts.request.UpdateLinkRequest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class LinkTests {

    Link sut;
    List<String> tags1;
    List<String> tags2;
    List<String> tags3;
    String tag1;
    String tag2;
    String tag3;
    String tag4;
    String tag5;

    @BeforeEach
    public void setup() {
        tag1 = "education";
        tag2 = "school";
        tag3 = "job";
        tag4 = "old";
        tag5 = "new";

        tags1 = new ArrayList<>();
        tags1.add(tag1);
        tags1.add(tag5);

        tags2 = new ArrayList<>();
        tags2.add(tag2);
        tags2.add(tag4);

        tags3 = new ArrayList<>();
        tags3.add(tag1);
        tags3.add(tag2);
        tags3.add(tag5);
    }

    @AfterEach
    public void cleanup() {
        sut = null;
    }

    @Test
    public void updateByRequest_WhenUpdateLinkRequestNull_ShouldNotChangeAnything() {
        //Arrange
        sut = new Link("1a45cca","www.google.com", "123456",null, "first link",tags1,0);

        //Act
        sut.updateByRequest(null);

        //Assert
        assertThat(sut.getPassword()).isEqualTo("123456");
        assertThat(sut.getDescription()).isEqualTo("first link");
        assertThat(sut.getTags().get(0)).isEqualTo(tag1);
        assertThat(sut.getTags().get(1)).isEqualTo(tag5);
    }

    @Test
    public void updateByRequest_WhenUpdateLinkRequestHaveNullElements_ShouldNotChangeAnything() {
        //Arrange
        sut = new Link("1a45cca","www.google.com", "123456",null, "first link",tags1,0);
        UpdateLinkRequest updateLinkRequest = new UpdateLinkRequest(null,null,null,null);

        //Act
        sut.updateByRequest(updateLinkRequest);

        //Assert
        assertThat(sut.getPassword()).isEqualTo("123456");
        assertThat(sut.getDescription()).isEqualTo("first link");
        assertThat(sut.getTags().get(0)).isEqualTo(tag1);
        assertThat(sut.getTags().get(1)).isEqualTo(tag5);
    }

    @Test
    public void updateByRequest_WhenUpdateLinkRequestHaveNewDescription_ShouldUpdateDescription() {
        //Arrange
        sut = new Link("1a45cca","www.google.com", "123456",null, "first link",tags1,0);
        UpdateLinkRequest updateLinkRequest = new UpdateLinkRequest(null,null,"New Description",null);

        //Act
        sut.updateByRequest(updateLinkRequest);

        //Assert
        assertThat(sut.getDescription()).isEqualTo(updateLinkRequest.getDescription());
    }

    @Test
    public void updateByRequest_WhenUpdateLinkRequestHaveNotNewDescription_ShouldNotUpdateDescription() {
        //Arrange
        sut = new Link("1a45cca","www.google.com", "123456",null, "first link",tags1,0);
        UpdateLinkRequest updateLinkRequest = new UpdateLinkRequest(null,"1453",null,tags2);

        //Act
        sut.updateByRequest(updateLinkRequest);

        //Assert
        assertThat(sut.getDescription()).isEqualTo("first link");
    }

    @Test
    public void updateByRequest_WhenUpdateLinkRequestHaveNewPassword_ShouldUpdatePassword() {
        //Arrange
        sut = new Link("1a45cca","www.google.com", "123456",null, "first link",tags1,0);
        UpdateLinkRequest updateLinkRequest = new UpdateLinkRequest(null,"111111", null,null);

        //Act
        sut.updateByRequest(updateLinkRequest);

        //Assert
        assertThat(sut.getPassword()).isEqualTo(updateLinkRequest.getPassword());
    }

    @Test
    public void updateByRequest_WhenUpdateLinkRequestHaveNotNewPassword_ShouldNotUpdatePassword() {
        //Arrange
        sut = new Link("1a45cca","www.google.com", "123456",null, "first link",tags1,0);
        UpdateLinkRequest updateLinkRequest = new UpdateLinkRequest(null,null,"New Description",tags3);

        //Act
        sut.updateByRequest(updateLinkRequest);

        //Assert
        assertThat(sut.getPassword()).isEqualTo("123456");
    }


}
