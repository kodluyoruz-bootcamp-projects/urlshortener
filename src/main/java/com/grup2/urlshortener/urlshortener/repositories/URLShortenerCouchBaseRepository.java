package com.grup2.urlshortener.urlshortener.repositories;

import com.couchbase.client.core.error.DocumentNotFoundException;
import com.couchbase.client.java.Cluster;
import com.couchbase.client.java.Collection;
import com.couchbase.client.java.kv.GetResult;
import com.couchbase.client.java.query.QueryResult;
import com.grup2.urlshortener.urlshortener.contracts.request.AddLinkRequest;
import com.grup2.urlshortener.urlshortener.contracts.request.AddUserRequest;
import com.grup2.urlshortener.urlshortener.models.Link;
import com.grup2.urlshortener.urlshortener.models.User;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class URLShortenerCouchBaseRepository  implements URLShortenerRepository {

    private final Cluster couchbaseCluster;
    private final Collection userCollection;

    public URLShortenerCouchBaseRepository(Cluster couchbaseCluster, Collection userCollection) {
        this.couchbaseCluster = couchbaseCluster;
        this.userCollection = userCollection;
    }

    @Override
    public User findUserById(String id) {
        try{
            GetResult getResult = userCollection.get(id);
            User user = getResult.contentAs(User.class);
            return user;
        }catch (DocumentNotFoundException ex){
            return null;
        }
    }

    @Override
    public User insertUser(AddUserRequest addUserRequest) {
        String statement;
        statement = "Select id, name from group2user where email = '" + addUserRequest.getEmail() + "'";
        QueryResult query = this.couchbaseCluster.query(statement);
        List<User> users = query.rowsAs(User.class);

        if(users.size() > 0){
            return users.get(0);
        }

        User user = new User(addUserRequest);
        userCollection.insert(user.getId(), user);
        return user;
    }

    @Override
    public void deleteUserById(String id) {
        userCollection.remove(id);
    }

    @Override
    public void updateUser(User user) {
        userCollection.replace(user.getId(), user);
    }

    @Override
    public List<Link> findAllLinksByUserId(String userId) {
        User user = findUserById(userId);
        return user.getLinks();
    }

    @Override
    public void deleteLinkById(String userId, String linkId) {
        User user = findUserById(userId);
        user.deleteLink(linkId);
        updateUser(user);
    }

    @Override
    public Link insertLink(String userId, AddLinkRequest addLinkRequest) {
        User user = findUserById(userId);
        user.addLink(addLinkRequest);
        updateUser(user);
        return user.getLinks().get(user.getLinkCount() - 1);
    }
}
