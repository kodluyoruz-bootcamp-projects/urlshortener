package com.grup2.urlshortener.urlshortener.repositories;


import com.grup2.urlshortener.urlshortener.contracts.request.AddLinkRequest;
import com.grup2.urlshortener.urlshortener.contracts.request.AddUserRequest;
import com.grup2.urlshortener.urlshortener.models.Link;
import com.grup2.urlshortener.urlshortener.models.User;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class URLShortenerMockRepository implements URLShortenerRepository {
    @Override
    public User findUserById(String id) {
        List<Link> links = new ArrayList<Link>();
        links.add(new Link("1455","ww.google.com.tr","1223",null,"best link", null,5));
        links.add(new Link("1458","ww.amazon.com.tr","",null,"my link", null,25));
        return new User(id,"Ahmet","ahmet@gmail.com","1121211113112",links,2);
    }

    @Override
    public User insertUser(AddUserRequest addUserRequest) {
        List<Link> links = new ArrayList<Link>();
        return new User("111",addUserRequest.getName(),addUserRequest.getEmail(),"12111122113",links,0);
    }

    @Override
    public void deleteUserById(String id) {}

    @Override
    public void updateUser(User user) {}

    @Override
    public List<Link> findAllLinksByUserId(String userId) {
        List<Link> links = new ArrayList<Link>();
        links.add(new Link("1455","ww.google.com.tr","1223",null,"best link", null,38));
        links.add(new Link("1458","ww.amazon.com.tr","",null,"my link", null,0));
        return links;
    }

    @Override
    public void deleteLinkById(String userId, String linkId) {}

    @Override
    public Link insertLink(String userId, AddLinkRequest addLinkRequest) {
        return new Link("1455",addLinkRequest.getTargetLink(),addLinkRequest.getPassword(),addLinkRequest.getExpireDate(),addLinkRequest.getDescription(),addLinkRequest.getTags(),0);
    }
}
