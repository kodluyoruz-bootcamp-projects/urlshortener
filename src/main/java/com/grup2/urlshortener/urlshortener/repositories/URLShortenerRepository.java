package com.grup2.urlshortener.urlshortener.repositories;

import com.grup2.urlshortener.urlshortener.contracts.request.AddLinkRequest;
import com.grup2.urlshortener.urlshortener.contracts.request.AddUserRequest;
import com.grup2.urlshortener.urlshortener.models.Link;
import com.grup2.urlshortener.urlshortener.models.User;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public interface URLShortenerRepository {
    User findUserById(String id);
    User insertUser(AddUserRequest addUserRequest);
    void deleteUserById(String id);
    void updateUser(User user);
    List<Link> findAllLinksByUserId(String userId);
    void deleteLinkById(String userId, String linkId);
    Link insertLink(String userId, AddLinkRequest addLinkRequest);
}
