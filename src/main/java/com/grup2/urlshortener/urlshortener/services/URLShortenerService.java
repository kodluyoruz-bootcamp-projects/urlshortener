package com.grup2.urlshortener.urlshortener.services;

import com.grup2.urlshortener.urlshortener.contracts.request.AddLinkRequest;
import com.grup2.urlshortener.urlshortener.contracts.request.AddUserRequest;
import com.grup2.urlshortener.urlshortener.contracts.request.UpdateLinkRequest;
import com.grup2.urlshortener.urlshortener.contracts.request.UpdateUserRequest;
import com.grup2.urlshortener.urlshortener.contracts.response.LinkResponse;
import com.grup2.urlshortener.urlshortener.contracts.response.UserResponse;
import com.grup2.urlshortener.urlshortener.models.Link;
import com.grup2.urlshortener.urlshortener.models.User;
import com.grup2.urlshortener.urlshortener.repositories.URLShortenerRepository;
//import com.grup2.urlshortener.urlshortener.repositories.URLShortenerCouchBaseRepository;
import com.grup2.urlshortener.urlshortener.repositories.URLShortenerCouchBaseRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class URLShortenerService {

    private final URLShortenerRepository urlShortenerRepository;

    public URLShortenerService(URLShortenerCouchBaseRepository urlShortenerCouchBaseRepository) {
        this.urlShortenerRepository = urlShortenerCouchBaseRepository;
    }

    public UserResponse findUserById(String id) {
        return new UserResponse(urlShortenerRepository.findUserById(id));
    }

    public User createUser(AddUserRequest addUserRequest) {
        return urlShortenerRepository.insertUser(addUserRequest);
    }

    public void deleteUserById(String id) {
        urlShortenerRepository.deleteUserById(id);
    }

    public void updateUser(String id, UpdateUserRequest updateUserRequest) {
        User user = urlShortenerRepository.findUserById(id);
        user.updateByRequest(updateUserRequest);
        urlShortenerRepository.updateUser(user);
    }

    public List<LinkResponse> findAllLinksByUserId(String userId) {
        List<Link> links = urlShortenerRepository.findAllLinksByUserId(userId);
        List<LinkResponse> linkResponses = new ArrayList<>();

        for (int i = 0; i < links.size(); i++) {
            linkResponses.add(new LinkResponse(links.get(i)));
        }

        return linkResponses;
    }

    public LinkResponse findLinkById(String userId, String linkId) {
        User user = urlShortenerRepository.findUserById(userId);
        Link link = user.getLinkById(linkId);
        return new LinkResponse(link);
    }

    public void deleteLinkById(String userId, String linkId) {
        urlShortenerRepository.deleteLinkById(userId,linkId);
    }

    public void updateLinkById(String userId, String linkId, UpdateLinkRequest updateLinkRequest) {
        User user = urlShortenerRepository.findUserById(userId);
        user.updateLink(linkId,updateLinkRequest);
        urlShortenerRepository.updateUser(user);
    }

    public Link createLink(String userId, AddLinkRequest addLinkRequest) {
        return urlShortenerRepository.insertLink(userId,addLinkRequest);
    }
}
