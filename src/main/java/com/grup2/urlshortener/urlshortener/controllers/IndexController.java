package com.grup2.urlshortener.urlshortener.controllers;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.view.RedirectView;

@RequestMapping("/")
@RestController
@CrossOrigin(origins="*", allowedHeaders = "*")
public class IndexController {
    @GetMapping
    public RedirectView home() {
        return new RedirectView("swagger-ui.html");
    }
}
