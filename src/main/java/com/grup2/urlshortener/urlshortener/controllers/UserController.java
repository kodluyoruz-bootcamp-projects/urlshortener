package com.grup2.urlshortener.urlshortener.controllers;

import com.grup2.urlshortener.urlshortener.contracts.request.AddUserRequest;
import com.grup2.urlshortener.urlshortener.contracts.request.UpdateUserRequest;
import com.grup2.urlshortener.urlshortener.contracts.response.UserResponse;
import com.grup2.urlshortener.urlshortener.exceptions.IdNotFoundException;
import com.grup2.urlshortener.urlshortener.exceptions.InvalidIdException;
import com.grup2.urlshortener.urlshortener.exceptions.RequestMissingInformationException;
import com.grup2.urlshortener.urlshortener.models.User;
import com.grup2.urlshortener.urlshortener.services.URLShortenerService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;

@RestController
@RequestMapping("/v1/users")
@CrossOrigin(origins="*", allowedHeaders = "*")
public class UserController {

    private final URLShortenerService urlShortenerService;

    public UserController(URLShortenerService urlShortenerService) {
        this.urlShortenerService = urlShortenerService;
    }

    @GetMapping("/{id}")
    public ResponseEntity getUser(@PathVariable String id) {

        if (id.contains("*")) {
            throw new InvalidIdException();
        }

        try{
            UserResponse response = this.urlShortenerService.findUserById(id);
            return ResponseEntity.ok(response);
        }catch (Exception ex){
            throw new IdNotFoundException();
        }
    }

    @PostMapping
    public ResponseEntity createUser(@RequestBody AddUserRequest addUserRequest) {

        if(addUserRequest == null || !addUserRequest.isRequestValid()){
            throw new RequestMissingInformationException();
        }

        User user = this.urlShortenerService.createUser(addUserRequest);

        URI location = URI.create(String.format("/users/%s", user.getId()));

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Access-Control-Expose-Headers",
                "*");

        return ResponseEntity.created(location)
                .headers(responseHeaders).build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteUser(@PathVariable String id) {

        if (id.contains("*")) {
            throw new InvalidIdException();
        }

        try{
            this.urlShortenerService.deleteUserById(id);
            return ResponseEntity.noContent().build();
        }catch (Exception ex){
            throw new IdNotFoundException();
        }

    }

    @PatchMapping("/{id}")
    public ResponseEntity updateUser(@PathVariable String id, @RequestBody(required = false) UpdateUserRequest updateUserRequest) {

        if (id.contains("*")) {
            throw new InvalidIdException();
        }

        try{
            this.urlShortenerService.updateUser(id,updateUserRequest);
            return ResponseEntity.noContent().build();
        }catch (Exception ex){
            throw new IdNotFoundException();
        }

    }

}
