package com.grup2.urlshortener.urlshortener.controllers;

import com.grup2.urlshortener.urlshortener.contracts.request.AddLinkRequest;
import com.grup2.urlshortener.urlshortener.contracts.request.AddUserRequest;
import com.grup2.urlshortener.urlshortener.contracts.request.UpdateLinkRequest;
import com.grup2.urlshortener.urlshortener.contracts.request.UpdateUserRequest;
import com.grup2.urlshortener.urlshortener.contracts.response.LinkResponse;
import com.grup2.urlshortener.urlshortener.contracts.response.UserResponse;
import com.grup2.urlshortener.urlshortener.exceptions.IdNotFoundException;
import com.grup2.urlshortener.urlshortener.exceptions.InvalidIdException;
import com.grup2.urlshortener.urlshortener.exceptions.RequestMissingInformationException;
import com.grup2.urlshortener.urlshortener.models.Link;
import com.grup2.urlshortener.urlshortener.models.User;
import com.grup2.urlshortener.urlshortener.services.URLShortenerService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/v1/users/{userId}/links")
@CrossOrigin(origins="*", allowedHeaders = "*")
public class LinkController {

    private final URLShortenerService urlShortenerService;

    public LinkController(URLShortenerService urlShortenerService) {
        this.urlShortenerService = urlShortenerService;
    }

    @GetMapping
    public ResponseEntity<List<LinkResponse>> getAllLinks(@PathVariable String userId) {

        if (userId.contains("*")) {
            throw new InvalidIdException();
        }

        try{
            List<LinkResponse> linkResponseslists = this.urlShortenerService.findAllLinksByUserId(userId);
            return ResponseEntity.ok(linkResponseslists);
        }catch (NullPointerException ex){
            throw new IdNotFoundException();
        }
    }

    @GetMapping("/{linkId}")
    public ResponseEntity<LinkResponse> getLink(@PathVariable String userId, @PathVariable String linkId) {

        if (userId.contains("*") || linkId.contains("*")) {
            throw new InvalidIdException();
        }

        try{
            LinkResponse response = this.urlShortenerService.findLinkById(userId,linkId);
            return ResponseEntity.ok(response);
        }catch (NullPointerException ex){
            throw new IdNotFoundException();
        }
    }

    @PostMapping
    public ResponseEntity createLink(@PathVariable String userId, @RequestBody AddLinkRequest addLinkRequest) {
        if(addLinkRequest == null || !addLinkRequest.isRequestValid()){
            throw new RequestMissingInformationException();
        }

        Link link = this.urlShortenerService.createLink(userId,addLinkRequest);

        URI location = URI.create(String.format("/users/%s/links/%s", userId,link.getId()));

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Access-Control-Expose-Headers",
                "*");

        return ResponseEntity.created(location)
                .headers(responseHeaders).build();
    }

    @DeleteMapping("/{linkId}")
    public ResponseEntity deleteLink(@PathVariable String userId, @PathVariable String linkId) {

        if (userId.contains("*") || linkId.contains("*")) {
            throw new InvalidIdException();
        }

        try{
            this.urlShortenerService.deleteLinkById(userId,linkId);
            return ResponseEntity.noContent().build();
        }catch (Exception ex){
            throw new IdNotFoundException();
        }
    }

    @PatchMapping("/{linkId}")
    public ResponseEntity updateLink(@PathVariable String userId, @PathVariable String linkId, @RequestBody(required = false) UpdateLinkRequest updateLinkRequest) {
        if (userId.contains("*") || linkId.contains("*")) {
            throw new InvalidIdException();
        }

        try{
            this.urlShortenerService.updateLinkById(userId,linkId,updateLinkRequest);
            return ResponseEntity.noContent().build();
        }catch (Exception ex){
            throw new IdNotFoundException();
        }
    }
}
