package com.grup2.urlshortener.urlshortener.models;

import com.grup2.urlshortener.urlshortener.contracts.request.AddLinkRequest;
import com.grup2.urlshortener.urlshortener.contracts.request.UpdateLinkRequest;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hashids.Hashids;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Link {

    private String id;
    private String targetLink;
    private String password;
    private Date expireDate;
    private String description;
    private List<String> tags;
    private int usageCounter;

    public Link(String targetLink, String password, Date expireDate, String description, List<String> tags) {
        String randomUUID = UUID.randomUUID().toString();
        Hashids hashids = new Hashids(randomUUID);
        this.id = hashids.encode(5L, 5L, 5L, 5L);
        this.targetLink = targetLink;
        this.password = password;
        this.expireDate = expireDate;
        this.description = description;
        this.tags = tags;
        this.usageCounter = 0;
    }

    public Link(AddLinkRequest addLinkRequest) {
        this(addLinkRequest.getTargetLink(),
                addLinkRequest.getPassword(),
                addLinkRequest.getExpireDate(),
                addLinkRequest.getDescription(),
                addLinkRequest.getTags());
    }

    public void updateByRequest(UpdateLinkRequest updateLinkRequest) {
        if(updateLinkRequest != null){
            if(updateLinkRequest.getExpireDate() != null){
                this.expireDate = updateLinkRequest.getExpireDate();
            }

            if(updateLinkRequest.getPassword() != null){
                this.password = updateLinkRequest.getPassword();
            }

            if(updateLinkRequest.getDescription() != null){
                this.description = updateLinkRequest.getDescription();
            }

            if(updateLinkRequest.getTags() != null){
                this.tags = updateLinkRequest.getTags();
            }
        }
    }
}
