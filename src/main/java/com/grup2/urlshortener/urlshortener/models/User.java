package com.grup2.urlshortener.urlshortener.models;

import com.grup2.urlshortener.urlshortener.contracts.request.AddLinkRequest;
import com.grup2.urlshortener.urlshortener.contracts.request.AddUserRequest;
import com.grup2.urlshortener.urlshortener.contracts.request.UpdateLinkRequest;
import com.grup2.urlshortener.urlshortener.contracts.request.UpdateUserRequest;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class User {
    private String id;
    private String name;
    private String email;
    private String token;
    private List<Link> links;
    private int linkCount;

    public User(AddUserRequest addUserRequest) {
        this(addUserRequest.getName(), addUserRequest.getEmail());
    }

    public User(String name, String email) {
        this.id = UUID.randomUUID().toString();
        this.name = name;
        this.email = email;
        this.links = new ArrayList<>();
        this.linkCount = 0;
    }

    public void updateByRequest(UpdateUserRequest updateUserRequest) {
        if(updateUserRequest != null){
            if(updateUserRequest.getName() != null){
                this.name = updateUserRequest.getName();
            }

            if(updateUserRequest.getEmail() != null){
                this.email = updateUserRequest.getEmail();
            }
        }
    }

    public void addLink(AddLinkRequest addLinkRequest){
        links.add(new Link(addLinkRequest));
        linkCount++;
    }

    public void deleteLink(String linkId) {
        for (int i = 0; i < this.getLinkCount(); i++) {
            if(this.getLinks().get(i).getId().equals(linkId)){
                this.getLinks().remove(i);
                linkCount--;
                break;
            }
        }
    }

    public void updateLink(String linkId, UpdateLinkRequest updateLinkRequest) {
        for (int i = 0; i < this.getLinkCount(); i++) {
            if(this.getLinks().get(i).getId().equals(linkId)){
                this.getLinks().get(i).updateByRequest(updateLinkRequest);
                break;
            }
        }
    }

    public Link getLinkById(String linkId) {
        for (int i = 0; i < this.getLinkCount(); i++) {
            if(this.getLinks().get(i).getId().equals(linkId)){
                return this.getLinks().get(i);
            }
        }
        return null;
    }
}
