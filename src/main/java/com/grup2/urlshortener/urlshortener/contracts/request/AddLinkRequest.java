package com.grup2.urlshortener.urlshortener.contracts.request;

import io.swagger.v3.oas.annotations.Hidden;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AddLinkRequest {
    private String targetLink;
    private Date expireDate;
    private String password;
    private String description;
    private List<String> tags;

    @Hidden
    public boolean isRequestValid() {
        return (targetLink != null);
    }
}
