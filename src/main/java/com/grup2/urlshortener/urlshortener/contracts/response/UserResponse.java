package com.grup2.urlshortener.urlshortener.contracts.response;

import com.grup2.urlshortener.urlshortener.models.Link;
import com.grup2.urlshortener.urlshortener.models.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserResponse {
    private String id;
    private String name;
    private String email;
    private String token;
    private List<LinkResponse> links;
    private int linkCount;

    public UserResponse(User user){

        this(user.getId(),
                user.getName(),
                user.getEmail(),
                user.getToken(),
                new ArrayList<>(),
                user.getLinkCount());

        for (int i = 0; i < user.getLinks().size(); i++) {
            this.links.add(new LinkResponse(user.getLinks().get(i)));
        }

    }
}
