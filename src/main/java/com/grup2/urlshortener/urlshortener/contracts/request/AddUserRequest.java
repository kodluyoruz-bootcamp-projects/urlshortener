package com.grup2.urlshortener.urlshortener.contracts.request;

import io.swagger.v3.oas.annotations.Hidden;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AddUserRequest {
    private String name;
    private String email;

    @Hidden
    public boolean isRequestValid() {
        return (name != null && email != null);
    }
}
