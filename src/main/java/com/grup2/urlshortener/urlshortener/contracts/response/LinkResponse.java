package com.grup2.urlshortener.urlshortener.contracts.response;

import com.grup2.urlshortener.urlshortener.models.Link;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class LinkResponse {
    private String id;
    private String targetLink;
    private String password;
    private Date expireDate;
    private String description;
    private List<String> tags;
    private int usageCounter;

    public LinkResponse(Link link){
        this(link.getId(),
                link.getTargetLink(),
                link.getPassword(),
                link.getExpireDate(),
                link.getDescription(),
                link.getTags(),
                link.getUsageCounter());
    }
}
