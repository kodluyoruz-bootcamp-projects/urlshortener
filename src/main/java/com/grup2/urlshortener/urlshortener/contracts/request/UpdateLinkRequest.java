package com.grup2.urlshortener.urlshortener.contracts.request;

import java.util.Date;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UpdateLinkRequest {
    private Date expireDate;
    private String password;
    private String description;
    private List<String> tags;
}
